using System.Net;
using Microsoft.JSInterop;
using Newtonsoft.Json;

namespace UrlShortener.Middleware.Tools
{
    public class ExceptionDetails
    {
        public string Message { get; set; }
        public int StatusCode { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}