using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Npgsql;
using Serilog;
using UrlShortener.Exceptions.AuthExceptions;
using UrlShortener.Middleware.Tools;

namespace UrlShortener.Middleware.ExceptionMiddleware.AuthMiddleware
{
    public class UnauthorizedMiddleware
    {
        private readonly RequestDelegate next;

        public UnauthorizedMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (AuthException e)
            {
                Log.Information("{e}", e);
                await HandleAuthException(context, e);
            }
        }
        
        private static Task HandleAuthException(HttpContext context, AuthException e)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) HttpStatusCode.Unauthorized;

            return context.Response.WriteAsync(new ExceptionDetails
            {
                Message = e.Message, StatusCode = context.Response.StatusCode
            }.ToString());
        }
    }
}