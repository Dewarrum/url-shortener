using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Npgsql;
using Serilog;
using UrlShortener.Middleware.Tools;

namespace UrlShortener.Middleware.ExceptionMiddleware.PostgresMiddleware
{
    public class PostgresDefaultExceptionMiddleware
    {
        private readonly RequestDelegate next;

        public PostgresDefaultExceptionMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (PostgresException e)
            {
                Log.Information("{e}", e);
                await HandleExceptionAsync(context, e);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, PostgresException e)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) HttpStatusCode.Conflict;

            return context.Response.WriteAsync(new ExceptionDetails
            {
                Message = e.Message, StatusCode = context.Response.StatusCode
            }.ToString());
        }
    }
}