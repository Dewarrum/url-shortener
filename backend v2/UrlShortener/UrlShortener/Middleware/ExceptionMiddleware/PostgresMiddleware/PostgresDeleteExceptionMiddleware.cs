using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog;
using UrlShortener.Exceptions.DatabaseExceptions;
using UrlShortener.Middleware.Tools;

namespace UrlShortener.Middleware.ExceptionMiddleware.PostgresMiddleware
{
    public class PostgresDeleteExceptionMiddleware
    {
        private readonly RequestDelegate next;

        public PostgresDeleteExceptionMiddleware(RequestDelegate next)
        {
            this.next = next;
        }
        
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (PostgresDeleteNotFoundException e)
            {
                Log.Information("{e}", e);
                await HandleExceptionAsync(context, e);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, PostgresDeleteNotFoundException e)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) HttpStatusCode.NotFound;

            return context.Response.WriteAsync(new ExceptionDetails
            {
                Message = e.Message, StatusCode = context.Response.StatusCode
            }.ToString());
        }
    }
}