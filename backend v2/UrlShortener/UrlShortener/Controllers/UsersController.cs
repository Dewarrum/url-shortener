using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Core;
using UrlShortener.Data.Models;
using UrlShortener.Data.Models.ApiModels;
using UrlShortener.Data.Models.ApiModels.ApiRequests.UserControllerRequests;
using UrlShortener.Data.Models.ApiModels.ApiResponses.UserControllerApiResponses;
using UrlShortener.Data.Models.DbModels;
using UrlShortener.Data.Repositories.UserRepository;

namespace UrlShortener.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    // [Authorize(Roles = "Admin")]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository userRepository;
        private readonly ILogger<UsersController> logger;

        public UsersController(IUserRepository userRepository, ILogger<UsersController> logger)
        {
            this.userRepository = userRepository;
            this.logger = logger;
        }
    
        [HttpGet("{name}")]
        public async Task<ActionResult<DbUser>> GetUserByName(string name)
        {
            logger.LogInformation("Requested user with name : {name}.", name);
            var user = await userRepository.GetUserByName(name);
            if (user == null)
            {
                logger.LogInformation("User with name : {name} could not be found.", name);
                return NotFound();
            }
            logger.LogInformation("User with name : {name} has been requested. {@user} returned.", name, user);
            return user;
        }

        [HttpGet]
        public async Task<object> GetAllUsers([FromQuery] int skip, int take)
        {
            var users = (await userRepository.GetAllUsers(skip, take)).ToList();
            
            logger.LogInformation("Requested user list. {@users} returned.", users);

            return new {users};
        }

        [HttpGet("{name}/info")]
        public async Task<ActionResult<DbAuthInfo>> GetUserInfo(string name)
        {
            var user = await userRepository.GetUserByName(name);
            if (user == null)
            {
                logger.LogInformation("User with name : {name} could not be found.", name);
                return NotFound();
            }
            var info = await userRepository.GetUserAuthInfo(user.Id);
            if (info == null)
            {
                logger.LogInformation("Info of user with name : {name} could not be found.", name);
                return NotFound();
            }
            logger.LogInformation("Requested {name}'s info. Returned {@info}", user.Name, info);
            return info;
        }

        [HttpGet("{name}/links")]
        public async Task<object> GetUsersLinksByName(string name)
        {
            var links = (await userRepository.GetUsersLinksByName(name)).ToList();
            
            logger.LogInformation("{name}'s links have been requested. {@links} returned.", name, links);

            return new {links};
        }

        [HttpPost("create")]
        public async Task<ActionResult<UserPostApiResponse>> CreateUser([FromBody] PostUserApiRequest request)
        {
            logger.LogInformation("Request to create user with requisites : {@request}.", request);
            var credentials = await userRepository.CreateUser(request.Name, request.Email, request.Password);
            logger.LogInformation("Created {@user} at {time}", credentials, DateTime.Now);
            return CreatedAtAction(nameof(GetUserByName), new {name = credentials.Name}, credentials);
        }

        [HttpDelete("{id}")]
        public async Task<DbUser> DeleteUserById(Guid id)
        {
            var user = await userRepository.DeleteUserById(id);
            logger.LogInformation("Deleted {@user}", user);
            return user;
        }
    }
}