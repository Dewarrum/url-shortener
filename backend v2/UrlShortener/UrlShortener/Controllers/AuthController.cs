using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UrlShortener.Data.Models.ApiModels.ApiRequests.AuthControllerRequests;
using UrlShortener.Data.Models.ApiModels.ApiResponses.AuthControllerResponses;
using UrlShortener.Data.Repositories.UserRepository;
using UrlShortener.Exceptions.DatabaseExceptions;
using UrlShortener.Security.Jwt.TokenDisassembling;
using UrlShortener.Services.Auth;

namespace UrlShortener.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUserRepository userRepository;
        private readonly IAuthService authService;
        private readonly ITokenDisassembler tokenDisassembler;

        public AuthController(IUserRepository userRepository, IAuthService authService,
            ITokenDisassembler tokenDisassembler)
        {
            this.userRepository = userRepository;
            this.authService = authService;
            this.tokenDisassembler = tokenDisassembler;
        }
        
        [HttpPost]
        [AllowAnonymous]
        public async Task<AuthTokenApiResponse> Auth([FromBody] AuthUserApiRequest request)
        {
            var user = await userRepository.GetUserByName(request.Name);
            if (user == null) throw new PostgresGetNotFoundException();
            
            var info = await userRepository.GetUserAuthInfo(user.Id);

            return await authService.AuthenticateUser(user, info, request);
        }

        [HttpPatch]
        public async Task<AuthRefreshTokenApiResponse> RefreshToken([FromBody] AuthRefreshTokenApiRequest request)
        {
            var username = tokenDisassembler.Disassemble(request.AccessToken).Identity.Name;
            var user = await userRepository.GetUserByName(username);
            if (user == null) throw new PostgresGetNotFoundException();
            
            var info = await userRepository.GetUserAuthInfo(user.Id);

            return await authService.RefreshUserToken(request.RefreshToken, user, info);
        }
    }
}