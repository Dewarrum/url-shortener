﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using UrlShortener.Data.Models;
using UrlShortener.Data.Models.DbModels;
using UrlShortener.Data.Repositories;
using UrlShortener.Data.Repositories.UserRepository;
using UrlShortener.Services.ShortLinkService;

namespace UrlShortener.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IUserRepository userRepository;
        private readonly ILogger logger;

        public ValuesController(IUserRepository userRepository, ILogger logger)
        {
            this.userRepository = userRepository;
            this.logger = logger;
        }
        
        [HttpGet]
        public async Task<DbAuthInfo> GetUserByName([FromBody] Guid id)
        {
            var info = await userRepository.GetUserAuthInfo(id);
            logger.Information("{id}, {@info}", id, info);

            logger.Information("{@info} returned.", info);
            return info;
        }
    }
}