using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using UrlShortener.Data.Repositories.LinkRepository;

namespace UrlShortener.Controllers
{
    [Route("")]
    [ApiController]
    public class RedirectController : ControllerBase
    {
        private readonly ILinkRepository linkRepository;
        private readonly ILogger logger;

        public RedirectController(ILinkRepository linkRepository, ILogger logger)
        {
            this.linkRepository = linkRepository;
            this.logger = logger;
        }

        [HttpGet("{code}")]
        public async Task<IActionResult> RedirectByCode(string code)
        {
            var link = await linkRepository.GetLinkByCode(code);
            logger.Information("Redirecting to {originalUrl} by request.", link.OriginalUrl);
            return Redirect(link.OriginalUrl);
        }
    }
}