using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using UrlShortener.Data.Models.ApiModels.ApiData;
using UrlShortener.Data.Models.ApiModels.ApiRequests.LinkControllerRequests;
using UrlShortener.Data.Models.ApiModels.ApiResponses.LinkControllerApiResponses;
using UrlShortener.Data.Models.DbModels;
using UrlShortener.Data.Repositories.LinkRepository;
using UrlShortener.Mapping;
using ILogger = Serilog.ILogger;

namespace UrlShortener.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    // [Authorize]
    public class LinksController : ControllerBase
    {
        private readonly ILinkRepository linkRepository;
        private readonly ILogger<LinksController> logger;

        public LinksController(ILinkRepository linkRepository, ILogger<LinksController> logger)
        {
            this.linkRepository = linkRepository;
            this.logger = logger;
        }

        [HttpGet]
        public async Task<GetLinksApiResponse> GetAllLinks([FromQuery] int skip, int take)
        {
            logger.LogInformation("Links with params {@params} have been requested.", new {skip, take});
            var links = (await linkRepository.GetAllLinks(skip, take)).Select(l => l.ToApi()).ToList();
            logger.LogInformation("{@links} have been returned.", links);

            return new GetLinksApiResponse() {Links = links};
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public async Task<ActionResult<ApiLink>> CreateLink([FromBody] PostLinkApiRequest request)
        {
            logger.LogInformation("Request to create link with url : {url}", request.OriginalUrl);
            var result = await linkRepository.CreateLink(request.OriginalUrl, request.UserId);
            logger.LogInformation("{@result} has been created.", result);

            return CreatedAtAction(nameof(GetLinkById), new {id = result.Id}, result.ToApi());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ApiLink>> GetLinkById(Guid id)
        {
            logger.LogInformation("Requested link with id : {id}", id);
            var result = await linkRepository.GetLinkById(id);
            if (result == null)
            {
                logger.LogInformation("Link with id : {id} has not been found.", id);
                return NotFound();
            }
            
            logger.LogInformation("{@link} has been returned.", result);

            return Ok(result);
        }

        [HttpGet("{id}/author")]
        public async Task<DbUser> GetLinksCreator(Guid id)
        {
            logger.LogInformation("Request on creator of link with id: {id}", id);
            var user = await linkRepository.GetLinksCreator(id);
            logger.LogInformation("Creator {@user} has been returned.", id, user);

            return user;
        }

        [HttpGet("url")]
        public async Task<GetLinksApiResponse> GetAllLinksByUrl([FromQuery] string url)
        {
            logger.LogInformation("Link with url: {url} have been requested.", url);
            var links = (await linkRepository.GetAllLinksByUrl(url)).Select(l => l.ToApi()).ToList();
            logger.LogInformation("Returned {@links} at time {time}", links, DateTime.Now);

            return new GetLinksApiResponse {Links = links};
        }

        [HttpDelete("{id}")]
        public async Task<DbLink> DeleteLinkById(Guid id)
        {
            logger.LogInformation("Request to delete link with id: {id}.", id);
            var link = await linkRepository.DeleteLinkById(id);
            logger.LogInformation("Link ID: [{linkId}] has been deleted.", link.Id);

            return link;
        }

        /* [HttpDelete("{code}")]
        public async Task<DbLink> DeleteLinkByCode(string code)
        {
            logger.LogInformation("Request to delete link with code: {code}.", code);
            var link = await linkRepository.DeleteLinkByCode(code);
            logger.LogInformation("Link [{code}] has been deleted.", code);

            return link;
        } */
    }
}