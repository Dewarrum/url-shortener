namespace UrlShortener.Exceptions.AuthExceptions
{
    public class WrongRefreshTokenException : AuthException
    {
        public WrongRefreshTokenException()
        : base("Wrong refresh token.")
        {
            
        }
    }
}