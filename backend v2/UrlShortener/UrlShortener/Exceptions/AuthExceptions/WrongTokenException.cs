using System;

namespace UrlShortener.Exceptions.AuthExceptions
{
    public class WrongTokenException : Exception
    {
        public WrongTokenException()
        : base("Wrong token.")
        {
            
        }
    }
}