using System;

namespace UrlShortener.Exceptions.AuthExceptions
{
    public class AuthException : Exception
    {
        protected AuthException(string message = "")
        : base(message)
        {
            
        }
    }
}