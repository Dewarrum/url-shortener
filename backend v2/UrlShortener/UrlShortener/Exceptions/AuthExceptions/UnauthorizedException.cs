using System;

namespace UrlShortener.Exceptions.AuthExceptions
{
    public class UnauthorizedException : AuthException
    {
        public UnauthorizedException()
        : base("Invalid login or password.")
        {
            
        }
    }
}