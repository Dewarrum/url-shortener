using System;

namespace UrlShortener.Exceptions.AuthExceptions
{
    public class RefreshTokenOutdatedException : AuthException
    {
        public RefreshTokenOutdatedException()
        : base("Refresh token has been outdated.")
        {
            
        }
    }
}