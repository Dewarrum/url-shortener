using System;

namespace UrlShortener.Exceptions.AuthExceptions
{
    public class NewIpException : AuthException
    {
        public NewIpException()
        : base("IP address changed. Authenticate again, please.")
        {
            
        }
    }
}