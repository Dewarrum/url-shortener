using System;

namespace UrlShortener.Exceptions.DatabaseExceptions
{
    public class PostgresGetNotFoundException : Exception
    {
        public PostgresGetNotFoundException()
        : base("Could not find because data could not be found with these parameters")
        {
            
        }
    }
}