using System;

namespace UrlShortener.Exceptions.DatabaseExceptions
{
    public class PostgresDeleteNotFoundException : Exception
    {
        public PostgresDeleteNotFoundException()
        : base("Could not delete because data could not be found with these parameters.")
        {
            
        }
    }
}