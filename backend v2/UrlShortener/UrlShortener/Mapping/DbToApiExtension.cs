using UrlShortener.Data.Models.ApiModels.ApiData;
using UrlShortener.Data.Models.DbModels;

namespace UrlShortener.Mapping
{
    public static class DbToApiExtension
    {
        public static ApiLink ToApi(this DbLink link)
        {
            return new ApiLink {OriginalUrl = link.OriginalUrl, ShortUrl = "http://localhost:5000/" + link.Code};
        }

        public static ApiUser ToApi(this DbUser user)
        {
            return new ApiUser {Email = user.Email, Name = user.Name};
        }
    }
}