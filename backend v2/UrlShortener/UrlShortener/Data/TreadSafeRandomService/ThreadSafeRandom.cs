using System;

namespace UrlShortener.Data.TreadSafeRandomService
{
    class ThreadSafeRandom
    {
        [ThreadStatic] private static Random random;
        public static byte[] NextBytes(int count)
        {
            random ??= new Random(Guid.NewGuid().GetHashCode());
            var buffer = new byte[count];
            random.NextBytes(buffer);
            return buffer;
        }

        public static int Next(int max = int.MaxValue)
        {
            random ??= new Random(Guid.NewGuid().GetHashCode());
            return random.Next(max);
        }
    }
}