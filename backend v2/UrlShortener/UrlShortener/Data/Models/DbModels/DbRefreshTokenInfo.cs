using System;
using System.Collections.Generic;

namespace UrlShortener.Data.Models.DbModels
{
    public class DbRefreshTokenInfo
    {
        public Dictionary<string, string> RefreshTokensIpMap { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}