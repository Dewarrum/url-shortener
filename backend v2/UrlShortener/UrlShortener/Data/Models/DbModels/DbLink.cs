using System;

namespace UrlShortener.Data.Models.DbModels
{
    public class DbLink
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string OriginalUrl { get; set; }
    }
}