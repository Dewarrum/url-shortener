using System;

namespace UrlShortener.Data.Models.DbModels
{
    public class DbUser
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}