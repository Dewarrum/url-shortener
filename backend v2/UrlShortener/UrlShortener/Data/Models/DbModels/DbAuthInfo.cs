using System;
using System.Collections.Generic;

namespace UrlShortener.Data.Models.DbModels
{
    public class DbAuthInfo
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public byte[] Password { get; set; }
        public byte[] Salt { get; set; }
        public string Role { get; set; }
        public DateTime ExpirationDate { get; set; }
        // Ip -- Key , RefreshToken -- Value
        public Dictionary<string, string> RefreshTokensIpMap { get; set; }
    }
}