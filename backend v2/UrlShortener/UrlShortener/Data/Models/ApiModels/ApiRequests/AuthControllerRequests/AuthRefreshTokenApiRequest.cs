namespace UrlShortener.Data.Models.ApiModels.ApiRequests.AuthControllerRequests
{
    public class AuthRefreshTokenApiRequest
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}