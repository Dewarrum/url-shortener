namespace UrlShortener.Data.Models.ApiModels.ApiRequests.AuthControllerRequests
{
    public class AuthUserApiRequest
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}