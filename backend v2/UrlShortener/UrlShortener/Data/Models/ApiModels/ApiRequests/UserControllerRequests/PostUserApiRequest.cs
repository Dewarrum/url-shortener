using System.ComponentModel;

namespace UrlShortener.Data.Models.ApiModels.ApiRequests.UserControllerRequests
{
    public class PostUserApiRequest
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}