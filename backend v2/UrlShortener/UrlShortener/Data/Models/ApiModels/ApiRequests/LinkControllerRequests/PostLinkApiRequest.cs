using System;

namespace UrlShortener.Data.Models.ApiModels.ApiRequests.LinkControllerRequests
{
    public class PostLinkApiRequest
    {
        public string OriginalUrl { get; set; }
        public Guid UserId { get; set; }
    }
}