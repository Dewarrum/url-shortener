namespace UrlShortener.Data.Models.ApiModels.ApiData
{
    public class ApiUser
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}