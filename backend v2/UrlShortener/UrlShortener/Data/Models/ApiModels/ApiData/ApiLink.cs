namespace UrlShortener.Data.Models.ApiModels.ApiData
{
    public class ApiLink
    {
        public string OriginalUrl { get; set; }
        public string ShortUrl { get; set; }
    }
}