using System.Collections.Generic;
using UrlShortener.Data.Models.ApiModels.ApiData;

namespace UrlShortener.Data.Models.ApiModels.ApiResponses.LinkControllerApiResponses
{
    public class GetLinksApiResponse
    {
        public IEnumerable<ApiLink> Links { get; set; }
    }
}