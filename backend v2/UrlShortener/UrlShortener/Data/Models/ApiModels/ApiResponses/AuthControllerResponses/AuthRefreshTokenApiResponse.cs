using System;
using System.Collections.Generic;

namespace UrlShortener.Data.Models.ApiModels.ApiResponses.AuthControllerResponses
{
    public class AuthRefreshTokenApiResponse
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}