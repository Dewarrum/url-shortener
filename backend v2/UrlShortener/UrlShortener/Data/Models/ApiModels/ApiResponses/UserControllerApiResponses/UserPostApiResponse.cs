using System;
using System.Collections.Generic;

namespace UrlShortener.Data.Models.ApiModels.ApiResponses.UserControllerApiResponses
{
    public class UserPostApiResponse
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string RefreshToken { get; set; }
        public DateTime Expires { get; set; }
    }
}