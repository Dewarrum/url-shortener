using System.Collections.Generic;
using UrlShortener.Data.Models.ApiModels.ApiData;
using UrlShortener.Data.Models.DbModels;

namespace UrlShortener.Data.Models.ApiModels.ApiResponses.UserControllerApiResponses
{
    public class GetUsersApiResponse
    {
        public IEnumerable<ApiUser> Users { get; set; }
    }
}