namespace UrlShortener.Data.Repositories.UserRepository
{
    public partial class UserRepository
    {
        private const string GetUserByNameSql = @"SELECT id, name, email
FROM users
WHERE name = @name";

        private const string CreateUserSql = @"BEGIN;

INSERT INTO users (id, name, email)
VALUES (@id, @name, @email);

INSERT INTO auth (id, user_id, password, salt, refresh_tokens_ip_map, role, refresh_token_expires)
VALUES (@id, @userId, @password, @salt, CAST(@refreshTokensIpMap AS json), @role, @time);

SELECT name, email, refresh_token_expires as expires, refresh_tokens_ip_map as refreshTokensIpMap
FROM users, auth
WHERE users.id = @id AND user_id = @id;

END;";

        private const string GetUserAuthInfoSql = @"SELECT id, user_id as userId, password, salt,
refresh_tokens_ip_map as refreshTokensIpMap, role, refresh_token_expires as expirationDate
FROM auth
WHERE user_id = @userId;";
        
        private const string GetUserByIdSql = @"SELECT id, name, email
FROM users
WHERE id = @id";

        private const string GetAllUsersSql = @"SELECT id, name, email
FROM users
OFFSET @skip
LIMIT @take";

        private const string GetUsersLinksSql = @"SELECT id, original_url as originalUrl, code
FROM links, user_link
WHERE user_link.user_id = @id";

        private const string GetUsersLinksByNameSql = @"SELECT links.id, original_url as originalUrl, code
FROM links, users, user_link
WHERE users.name = @name AND links.id = user_link.link_id AND users.id = user_link.user_id;";

        private const string DeleteUserByIdSql = @"DELETE
FROM users
WHERE id = @id
RETURNING id, name, email";
    }
}