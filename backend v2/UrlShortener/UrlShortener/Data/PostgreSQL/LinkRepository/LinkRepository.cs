namespace UrlShortener.Data.Repositories.LinkRepository
{
    public partial class LinkRepository
    {
        private const string GetAllLinksSql = @"SELECT id, original_url as originalUrl, code
FROM links
OFFSET @skip
LIMIT @take;";

        private const string CreateLinkSql = @"BEGIN;

INSERT INTO links (id, original_url, code)
VALUES (@id, @originalUrl, @code);

INSERT INTO user_link (user_id, link_id, creation_date)
VALUES (@userId, @linkId, @creationDate);

SELECT id, original_url as originalUrl, code
FROM links
WHERE id = @newLinkId;

END;";

        private const string CreateLinkAnonymouslySql = @"INSERT INTO links
VALUES (@id, @originalUrl, @code)
RETURNING id, original_url as originalUrl, code;";

        private const string GetLinksCreatorSql = @"SELECT users.id, name, email, role
FROM users, links, user_link
WHERE links.id = @linkId AND user_link.link_id = @linkId AND users.id = user_link.user_id";

        private const string GetAllLinksByUrlSql = @"SELECT id, original_url as originalUrl, code
FROM links
WHERE original_url = @originalUrl";

        private const string DeleteLinkByIdSql = @"DELETE FROM links
WHERE id = @id
RETURNING id, original_url as originalUrl, code";

        private const string DeleteLinkByCodeSql = @"DELETE FROM links
WHERE code = @code
RETURNING id, original_url as originalUrl, code";

        private const string GetLinkByIdSql = @"SELECT id, original_url as originalUrl, code
FROM links
WHERE id = @id";

        private const string GetLinkByCodeSql = @"SELECT id, original_url as originalUrl, code
FROM links
WHERE code = @code";
    }
}