namespace UrlShortener.Data.Repositories.AuthInfoRepository
{
    public partial class AuthInfoRepository
    {
        private const string UpdateUserRefreshTokenSql = @"UPDATE auth
SET refresh_tokens_ip_map = CAST(@refreshTokensIpMap AS jsonb) , refresh_token_expires = @time
WHERE user_id = @userId
RETURNING refresh_tokens_ip_map as refreshTokensIpMap, refresh_token_expires as expirationDate;";
    }
}