using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using UrlShortener.Data.Models.DbModels;

namespace UrlShortener.Data.Repositories.AuthInfoRepository
{
    partial class AuthInfoRepository : IAuthInfoRepository
    {
        private readonly Func<IDbConnection> dbConnectionFactory;

        public AuthInfoRepository(Func<IDbConnection> dbConnectionFactory)
        {
            this.dbConnectionFactory = dbConnectionFactory;
        }
        
        public async Task<DbRefreshTokenInfo> UpdateUserRefreshToken(Dictionary<string, string> refreshTokensIpMap, DateTime time, Guid userId)
        {
            using var db = dbConnectionFactory();

            var command = new CommandDefinition(UpdateUserRefreshTokenSql, new
            {
                refreshTokensIpMap, time, userId
            });

            return await db.QueryFirstOrDefaultAsync<DbRefreshTokenInfo>(command);
        }
    }
}