using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UrlShortener.Data.Models.DbModels;

namespace UrlShortener.Data.Repositories.AuthInfoRepository
{
    public interface IAuthInfoRepository
    {
        Task<DbRefreshTokenInfo> UpdateUserRefreshToken(Dictionary<string, string> refreshTokenIpMap, DateTime time,
            Guid userId);
    }
}