using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Http;
using UrlShortener.Data.Models.ApiModels.ApiResponses.AuthControllerResponses;
using UrlShortener.Data.Models.ApiModels.ApiResponses.UserControllerApiResponses;
using UrlShortener.Data.Models.DbModels;
using UrlShortener.Security.Jwt.JwtGeneration;
using UrlShortener.Security.Jwt.RefreshToken.RefreshTokenGeneration;
using UrlShortener.Security.Jwt.RefreshToken.RefreshTokenValidation;
using UrlShortener.Security.Jwt.TokenDisassembling;
using UrlShortener.Security.PasswordHashing;
using UrlShortener.Security.SaltGeneration;

namespace UrlShortener.Data.Repositories.UserRepository
{
    public partial class UserRepository : IUserRepository
    {
        private readonly Func<IDbConnection> dbConnectionFactory;
        private readonly IPasswordHasher passwordHasher;
        private readonly ISaltGenerator saltGenerator;
        private readonly IRefreshTokenGenerator refreshTokenGenerator;
        private readonly IHttpContextAccessor httpContextAccessor;

        public UserRepository(Func<IDbConnection> dbConnectionFactory, IPasswordHasher passwordHasher,
            ISaltGenerator saltGenerator, IHttpContextAccessor httpContextAccessor, IRefreshTokenGenerator refreshTokenGenerator)
        {
            this.dbConnectionFactory = dbConnectionFactory;
            this.passwordHasher = passwordHasher;
            this.saltGenerator = saltGenerator;
            this.httpContextAccessor = httpContextAccessor;
            this.refreshTokenGenerator = refreshTokenGenerator;
        }
        
        public async Task<DbUser> GetUserByName(string name)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(GetUserByNameSql, new {name});
            return await db.QueryFirstOrDefaultAsync<DbUser>(command);
        }

        public async Task<DbUser> GetUserById(Guid id)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(GetUserByIdSql, new {id});
            return await db.QueryFirstOrDefaultAsync<DbUser>(command);
        }

        public async Task<UserPostApiResponse> CreateUser(string name, string email, string password, string role = "User")
        {
            using var db = dbConnectionFactory();
            var userSalt = saltGenerator.GenerateSalt();
            var hashedPassword = passwordHasher.GetHash(password, userSalt);
            var refreshToken = refreshTokenGenerator.GenerateRefreshToken();
            var ip = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            var refreshTokensIpMap = new Dictionary<string, string>
            {
                [ip] = refreshToken
            };
            var userId = Guid.NewGuid();

            var command = new CommandDefinition(CreateUserSql, new
            {
                id = userId, name, email,
                
                userId, password = hashedPassword, salt = userSalt,
                refreshTokensIpMap, role, time = DateTime.UtcNow.AddDays(1)
            });

            var result = await db.QueryFirstOrDefaultAsync<UserPostApiResponse>(command);
            result.RefreshToken = refreshToken;

            return result;
        }

        public async Task<IEnumerable<DbUser>> GetAllUsers(int skip, int take)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(GetAllUsersSql, new {skip, take});
            return await db.QueryAsync<DbUser>(command);
        }

        public async Task<IEnumerable<DbLink>> GetUsersLinks(Guid id)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(GetUsersLinksSql, new {id});
            return await db.QueryAsync<DbLink>(command);
        }

        public async Task<IEnumerable<DbLink>> GetUsersLinksByName(string name)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(GetUsersLinksByNameSql, new {name});
            return await db.QueryAsync<DbLink>(command);
        }

        public async Task<DbUser> DeleteUserById(Guid id)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(DeleteUserByIdSql, new {id});
            return await db.QueryFirstOrDefaultAsync<DbUser>(command);
        }

        public async Task<DbAuthInfo> GetUserAuthInfo(Guid id)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(GetUserAuthInfoSql, new {userId = id});
            return await db.QueryFirstOrDefaultAsync<DbAuthInfo>(command);
        }
    }
}