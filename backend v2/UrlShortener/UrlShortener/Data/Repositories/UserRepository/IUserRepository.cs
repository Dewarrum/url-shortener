using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UrlShortener.Data.Models;
using UrlShortener.Data.Models.ApiModels.ApiResponses.AuthControllerResponses;
using UrlShortener.Data.Models.ApiModels.ApiResponses.UserControllerApiResponses;
using UrlShortener.Data.Models.DbModels;

namespace UrlShortener.Data.Repositories.UserRepository
{
    public interface IUserRepository
    {
        Task<DbUser> GetUserByName(string name);
        Task<DbUser> GetUserById(Guid id);
        Task<UserPostApiResponse> CreateUser(string name, string email, string password, string role = "User");
        Task<IEnumerable<DbUser>> GetAllUsers(int skip, int take);
        Task<IEnumerable<DbLink>> GetUsersLinks(Guid id);
        Task<IEnumerable<DbLink>> GetUsersLinksByName(string name);
        Task<DbUser> DeleteUserById(Guid id);
        Task<DbAuthInfo> GetUserAuthInfo(Guid id);
    }
}