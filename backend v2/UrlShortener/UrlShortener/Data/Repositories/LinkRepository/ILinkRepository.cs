using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UrlShortener.Data.Models;
using UrlShortener.Data.Models.DbModels;

namespace UrlShortener.Data.Repositories.LinkRepository
{
    public interface ILinkRepository
    {
        Task<List<DbLink>> GetAllLinks(int skip, int take);
        Task<DbLink> CreateLink(string originalUrl, Guid userId);
        Task<DbUser> GetLinksCreator(Guid id);
        Task<IEnumerable<DbLink>> GetAllLinksByUrl(string originalUrl);
        Task<DbLink> DeleteLinkById(Guid id);
        Task<DbLink> DeleteLinkByCode(string code);
        Task<DbLink> GetLinkByCode(string code);
        Task<DbLink> GetLinkById(Guid id);
    }
}