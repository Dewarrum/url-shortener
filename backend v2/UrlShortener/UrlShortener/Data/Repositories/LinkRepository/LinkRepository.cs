using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using UrlShortener.Data.Models.DbModels;
using UrlShortener.Exceptions.DatabaseExceptions;
using UrlShortener.Services.ShortLinkService;

namespace UrlShortener.Data.Repositories.LinkRepository
{
    public partial class LinkRepository : ILinkRepository
    {
        private readonly Func<IDbConnection> dbConnectionFactory;
        private readonly ILinkIdGenerator linkIdGenerator;

        public LinkRepository(Func<IDbConnection> dbConnectionFactory, ILinkIdGenerator linkIdGenerator)
        {
            this.dbConnectionFactory = dbConnectionFactory;
            this.linkIdGenerator = linkIdGenerator;
        }
        
        public async Task<List<DbLink>> GetAllLinks(int skip, int take)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(GetAllLinksSql, new {skip, take});

            return (await db.QueryAsync<DbLink>(command)).ToList();
        }

        public async Task<DbLink> CreateLink(string originalUrl, Guid userId)
        {
            using var db = dbConnectionFactory();
            var id = Guid.NewGuid();
            var code = linkIdGenerator.GenerateId();
            
            var command = userId != Guid.Empty ? new CommandDefinition(CreateLinkSql, new
            {
                id, originalUrl, code,
                userId, linkId = id, creationDate = DateTime.Now,
                newLinkId = id
            }) : new CommandDefinition(CreateLinkAnonymouslySql, new {id, originalUrl, code});

            return await db.QueryFirstOrDefaultAsync<DbLink>(command);
        }

        public async Task<DbUser> GetLinksCreator(Guid id)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(GetLinksCreatorSql, new {linkId = id});
            return await db.QueryFirstOrDefaultAsync<DbUser>(command);
        }

        public async Task<IEnumerable<DbLink>> GetAllLinksByUrl(string originalUrl)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(GetAllLinksByUrlSql, new {originalUrl});
            return await db.QueryAsync<DbLink>(command);
        }

        public async Task<DbLink> DeleteLinkById(Guid id)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(DeleteLinkByIdSql, new {id});
            var result =  await db.QueryFirstOrDefaultAsync<DbLink>(command);
            if (result == null) throw new PostgresDeleteNotFoundException();

            return result;
        }

        public async Task<DbLink> DeleteLinkByCode(string code)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(DeleteLinkByCodeSql, new {code});
            var result =  await db.QueryFirstOrDefaultAsync<DbLink>(command);
            if (result == null) throw new PostgresDeleteNotFoundException();

            return result;
        }

        public async Task<DbLink> GetLinkByCode(string code)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(GetLinkByCodeSql, new {code});
            return await db.QueryFirstOrDefaultAsync<DbLink>(command);
        }

        public async Task<DbLink> GetLinkById(Guid id)
        {
            using var db = dbConnectionFactory();
            var command = new CommandDefinition(GetLinkByIdSql, new {id});
            return await db.QueryFirstOrDefaultAsync<DbLink>(command);
        }
    }
}