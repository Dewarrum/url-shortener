using System;

namespace UrlShortener.Security.SaltGeneration
{
    public class SaltGenerator : ISaltGenerator
    {
        private const int SaltLength = 20;
        private readonly Random rnd = new Random();
        public byte[] GenerateSalt()
        {
            var result = new byte[SaltLength];
            rnd.NextBytes(result);
            return result;
        }
    }
}