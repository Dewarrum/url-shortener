namespace UrlShortener.Security.SaltGeneration
{
    public interface ISaltGenerator
    {
        byte[] GenerateSalt();
    }
}