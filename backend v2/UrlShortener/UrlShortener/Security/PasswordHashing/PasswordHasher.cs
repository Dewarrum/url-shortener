using System;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace UrlShortener.Security.PasswordHashing
{
    public class PasswordHasher : IPasswordHasher
    {
        private readonly byte[] appSalt = Encoding.UTF8.GetBytes("application-salt");
        public byte[] GetHash(string password, byte[] userSalt)
        {
            var result = KeyDerivation.Pbkdf2(password, userSalt,
                KeyDerivationPrf.HMACSHA512, 1000, 40);
            result = KeyDerivation.Pbkdf2(Convert.ToBase64String(result), appSalt,
                KeyDerivationPrf.HMACSHA512, 1000, 40);
            return result;
        }
    }
}