namespace UrlShortener.Security.PasswordHashing
{
    public interface IPasswordHasher
    {
        byte[] GetHash(string password, byte[] userSalt);
    }
}