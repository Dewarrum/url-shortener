using System;
using UrlShortener.Data.Models.DbModels;
using UrlShortener.Security.PasswordHashing;

namespace UrlShortener.Security.PasswordValidation
{
    public class PasswordValidator : IPasswordValidator
    {
        private readonly IPasswordHasher passwordHasher;

        public PasswordValidator(IPasswordHasher passwordHasher)
        {
            this.passwordHasher = passwordHasher;
        }
        
        public bool Validate(DbAuthInfo info, string password)
        {
            var candidate = new Span<byte>(passwordHasher.GetHash(password, info.Salt));
            var real = new Span<byte>(info.Password);

            return candidate.SequenceEqual(real);
        }
    }
}