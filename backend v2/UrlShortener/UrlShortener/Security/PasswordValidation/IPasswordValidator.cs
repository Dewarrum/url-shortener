using UrlShortener.Data.Models.DbModels;

namespace UrlShortener.Security.PasswordValidation
{
    public interface IPasswordValidator
    {
        bool Validate(DbAuthInfo info, string password);
    }
}