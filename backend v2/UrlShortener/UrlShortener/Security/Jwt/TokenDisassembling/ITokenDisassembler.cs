using System.Security.Claims;

namespace UrlShortener.Security.Jwt.TokenDisassembling
{
    public interface ITokenDisassembler
    {
        ClaimsPrincipal Disassemble(string token);
    }
}