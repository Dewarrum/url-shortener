using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using UrlShortener.Exceptions.AuthExceptions;
using UrlShortener.Security.Jwt.SecurityKeyProviding;

namespace UrlShortener.Security.Jwt.TokenDisassembling
{
    public class TokenDisassembler : ITokenDisassembler
    {
        private readonly ISecurityKeyProvider securityKeyProvider;

        public TokenDisassembler(ISecurityKeyProvider securityKeyProvider)
        {
            this.securityKeyProvider = securityKeyProvider;
        }
        
        public ClaimsPrincipal Disassemble(string token)
        {
            var tokenValidationParams = new TokenValidationParameters
            {
                ValidateAudience = false, ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = securityKeyProvider.GetSecurityKey(),
                ValidateLifetime = false
            };

            var principal =
                new JwtSecurityTokenHandler().ValidateToken(token, tokenValidationParams, out var securityToken);

            if (!(securityToken is JwtSecurityToken jwtSecurityToken) ||
                !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new WrongTokenException();

            return principal;
        }
    }
}