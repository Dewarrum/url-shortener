using System.Collections.Generic;

namespace UrlShortener.Security.Jwt.IpValidation
{
    public interface IIpValidator
    {
        bool Validate(string ip, Dictionary<string, string> whiteList);
    }
}