using System.Collections.Generic;

namespace UrlShortener.Security.Jwt.IpValidation
{
    class IpValidator : IIpValidator
    {
        public bool Validate(string ip, Dictionary<string, string> whiteList)
        {
            return whiteList.ContainsKey(ip);
        }
    }
}