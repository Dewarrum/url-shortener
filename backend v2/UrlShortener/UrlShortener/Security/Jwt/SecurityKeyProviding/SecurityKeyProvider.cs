using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace UrlShortener.Security.Jwt.SecurityKeyProviding
{
    public class SecurityKeyProvider : ISecurityKeyProvider
    {
        private readonly IConfiguration configuration;

        public SecurityKeyProvider(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        
        public SymmetricSecurityKey GetSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
        }
    }
}