using Microsoft.IdentityModel.Tokens;

namespace UrlShortener.Security.Jwt.SecurityKeyProviding
{
    public interface ISecurityKeyProvider
    {
        SymmetricSecurityKey GetSecurityKey();
    }
}