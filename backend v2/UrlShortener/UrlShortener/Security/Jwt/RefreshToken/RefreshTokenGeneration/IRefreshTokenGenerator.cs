namespace UrlShortener.Security.Jwt.RefreshToken.RefreshTokenGeneration
{
    public interface IRefreshTokenGenerator
    {
        string GenerateRefreshToken();
    }
}