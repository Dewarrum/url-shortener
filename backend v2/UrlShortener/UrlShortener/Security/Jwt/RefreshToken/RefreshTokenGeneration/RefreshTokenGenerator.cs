using System;
using System.Security.Cryptography;
using System.Text;

namespace UrlShortener.Security.Jwt.RefreshToken.RefreshTokenGeneration
{
    public class RefreshTokenGenerator : IRefreshTokenGenerator
    {
        public string GenerateRefreshToken()
        {
            var buffer = new byte[24];
            using var rng = RandomNumberGenerator.Create();
            rng.GetBytes(buffer);
            return Convert.ToBase64String(buffer);
        }
    }
}