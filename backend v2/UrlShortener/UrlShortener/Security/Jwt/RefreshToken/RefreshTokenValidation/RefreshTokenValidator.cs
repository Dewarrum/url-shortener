using System;
using System.Linq;
using System.Threading.Tasks;
using UrlShortener.Data.Models.DbModels;
using UrlShortener.Data.Repositories.UserRepository;
using UrlShortener.Exceptions.AuthExceptions;

namespace UrlShortener.Security.Jwt.RefreshToken.RefreshTokenValidation
{
    public class RefreshTokenValidator : IRefreshTokenValidator
    {
        public bool ValidateRefreshToken(string candidateRefreshToken, string remoteIp, DbAuthInfo info)
        {
            if (DateTime.Compare(info.ExpirationDate, DateTime.UtcNow) < 0)
                throw new RefreshTokenOutdatedException();

            var correctToken = info.RefreshTokensIpMap[remoteIp];

            return correctToken == candidateRefreshToken;
        }
    }
}