using System;
using System.Threading.Tasks;
using UrlShortener.Data.Models.DbModels;

namespace UrlShortener.Security.Jwt.RefreshToken.RefreshTokenValidation
{
    public interface IRefreshTokenValidator
    {
        bool ValidateRefreshToken(string candidateRefreshToken, string remoteIp, DbAuthInfo info);
    }
}