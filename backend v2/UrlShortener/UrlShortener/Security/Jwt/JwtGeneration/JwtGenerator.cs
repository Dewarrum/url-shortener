using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using UrlShortener.Data.Models.DbModels;
using UrlShortener.Security.Jwt.SecurityKeyProviding;

namespace UrlShortener.Security.Jwt.JwtGeneration
{
    public class JwtGenerator : IJwtGenerator
    {
        private readonly ISecurityKeyProvider securityKeyProvider;

        public JwtGenerator(ISecurityKeyProvider securityKeyProvider)
        {
            this.securityKeyProvider = securityKeyProvider;
        }
        
        public JwtSecurityToken GenerateToken(DbUser user, DbAuthInfo info)
        {
            var securityKey = securityKeyProvider.GetSecurityKey();
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.Role, info.Role)
            };

            var token = new JwtSecurityToken(expires: DateTime.UtcNow.AddMinutes(1), signingCredentials: credentials,
                claims: claims, notBefore: DateTime.UtcNow);
            return token;
        }
    }
}