using System.IdentityModel.Tokens.Jwt;
using UrlShortener.Data.Models.DbModels;

namespace UrlShortener.Security.Jwt.JwtGeneration
{
    public interface IJwtGenerator
    {
        JwtSecurityToken GenerateToken(DbUser user, DbAuthInfo info);
    }
}