using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using UrlShortener.Data.Models.ApiModels.ApiRequests.AuthControllerRequests;
using UrlShortener.Data.Models.ApiModels.ApiResponses.AuthControllerResponses;
using UrlShortener.Data.Models.DbModels;
using UrlShortener.Data.Repositories.AuthInfoRepository;
using UrlShortener.Exceptions.AuthExceptions;
using UrlShortener.Security.Jwt.IpValidation;
using UrlShortener.Security.Jwt.JwtGeneration;
using UrlShortener.Security.Jwt.RefreshToken.RefreshTokenGeneration;
using UrlShortener.Security.Jwt.RefreshToken.RefreshTokenValidation;
using UrlShortener.Security.PasswordValidation;

namespace UrlShortener.Services.Auth
{
    public class AuthService : IAuthService
    {
        private readonly IRefreshTokenValidator refreshTokenValidator;
        private readonly IRefreshTokenGenerator refreshTokenGenerator;
        private readonly IJwtGenerator jwtGenerator;
        private readonly IPasswordValidator passwordValidator;
        private readonly IIpValidator ipValidator;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IAuthInfoRepository authInfoRepository;

        public AuthService(IRefreshTokenValidator refreshTokenValidator,
            IRefreshTokenGenerator refreshTokenGenerator, IJwtGenerator jwtGenerator, IPasswordValidator passwordValidator,
            IIpValidator ipValidator, IHttpContextAccessor httpContextAccessor, IAuthInfoRepository authInfoRepository)
        {
            this.refreshTokenValidator = refreshTokenValidator;
            this.refreshTokenGenerator = refreshTokenGenerator;
            this.jwtGenerator = jwtGenerator;
            this.passwordValidator = passwordValidator;
            this.ipValidator = ipValidator;
            this.httpContextAccessor = httpContextAccessor;
            this.authInfoRepository = authInfoRepository;
        }
        
        public async Task<AuthRefreshTokenApiResponse> RefreshUserToken(string refreshToken, DbUser user, DbAuthInfo info)
        {
            var remoteIp = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            if (!ipValidator.Validate(remoteIp, info.RefreshTokensIpMap))
                throw new UnauthorizedException();
            
            if (!refreshTokenValidator.ValidateRefreshToken(refreshToken, remoteIp, info))
                throw new WrongRefreshTokenException();

            info.RefreshTokensIpMap.Remove(remoteIp);
            if (info.RefreshTokensIpMap.Count > 10)
                info.RefreshTokensIpMap = new Dictionary<string, string>();
            
            var newRefreshToken = refreshTokenGenerator.GenerateRefreshToken();
            info.RefreshTokensIpMap.Add(remoteIp, newRefreshToken);
            var newAccessToken = jwtGenerator.GenerateToken(user, info);
            var newRefreshTokenExpiration = DateTime.UtcNow.AddDays(1);
            
            var refreshTokenInfo = await authInfoRepository.
                UpdateUserRefreshToken(info.RefreshTokensIpMap, newRefreshTokenExpiration, user.Id);
            
            var result = new AuthRefreshTokenApiResponse
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(newAccessToken),
                ExpirationDate = refreshTokenInfo.ExpirationDate,
                RefreshToken = GetTokenFromMap(refreshTokenInfo.RefreshTokensIpMap)
            };
            return result;
        }

        public async Task<AuthTokenApiResponse> AuthenticateUser(DbUser user, DbAuthInfo info, AuthUserApiRequest request)
        {
            if (!passwordValidator.Validate(info, request.Password))
                throw new UnauthorizedException();

            var accessToken = jwtGenerator.GenerateToken(user, info);
            var newRefreshToken = refreshTokenGenerator.GenerateRefreshToken();
            var expires = DateTime.UtcNow.AddDays(1);
            var refreshTokensIpMap = RefreshTokenInMap(newRefreshToken, info.RefreshTokensIpMap);
            
            var refreshTokenInfo = await authInfoRepository.UpdateUserRefreshToken(refreshTokensIpMap, expires, user.Id);
            
            var token = GetTokenFromMap(refreshTokenInfo.RefreshTokensIpMap);
            var result = new AuthTokenApiResponse
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(accessToken),
                ExpirationDate = refreshTokenInfo.ExpirationDate,
                RefreshToken = token
            };
            return result;
        }

        private Dictionary<string, string> RefreshTokenInMap(string refreshToken, Dictionary<string, string> ipMap)
        {
            var ip = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            if (!ipMap.ContainsKey(ip))
            {
                ipMap.Add(ip, refreshToken);
                return ipMap;
            }
            ipMap[ip] = refreshToken;
            return ipMap;
        }

        private string GetTokenFromMap(Dictionary<string, string> ipMap)
        {
            var ip = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            if (!ipMap.ContainsKey(ip))
                throw new NewIpException();

            return ipMap[ip];
        }
    }
}