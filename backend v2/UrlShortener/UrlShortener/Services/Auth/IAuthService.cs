using System.Threading.Tasks;
using UrlShortener.Data.Models.ApiModels.ApiRequests.AuthControllerRequests;
using UrlShortener.Data.Models.ApiModels.ApiResponses.AuthControllerResponses;
using UrlShortener.Data.Models.DbModels;

namespace UrlShortener.Services.Auth
{
    public interface IAuthService
    {
        Task<AuthRefreshTokenApiResponse> RefreshUserToken(string refreshToken, DbUser user, DbAuthInfo info);
        Task<AuthTokenApiResponse> AuthenticateUser(DbUser user, DbAuthInfo info, AuthUserApiRequest request);
    }
}