using Grace.DependencyInjection;
using UrlShortener.Middleware.ExceptionMiddleware.AuthMiddleware;
using UrlShortener.Security.Jwt.IpValidation;
using UrlShortener.Security.Jwt.JwtGeneration;
using UrlShortener.Security.Jwt.RefreshToken.RefreshTokenGeneration;
using UrlShortener.Security.Jwt.RefreshToken.RefreshTokenValidation;
using UrlShortener.Security.Jwt.SecurityKeyProviding;
using UrlShortener.Security.Jwt.TokenDisassembling;
using UrlShortener.Security.PasswordHashing;
using UrlShortener.Security.PasswordValidation;
using UrlShortener.Security.SaltGeneration;
using UrlShortener.Services.Auth;

namespace UrlShortener.Services.Configurations
{
    public class SecurityConfigurationModule : IConfigurationModule
    {
        public void Configure(IExportRegistrationBlock r)
        {
            r.Export<PasswordHasher>().As<IPasswordHasher>().Lifestyle.Singleton();
            r.Export<SaltGenerator>().As<ISaltGenerator>().Lifestyle.Singleton();
            r.Export<PasswordValidator>().As<IPasswordValidator>();
            r.Export<JwtGenerator>().As<IJwtGenerator>();
            r.Export<RefreshTokenGenerator>().As<IRefreshTokenGenerator>();
            r.Export<RefreshTokenValidator>().As<IRefreshTokenValidator>();
            r.Export<TokenDisassembler>().As<ITokenDisassembler>();
            r.Export<SecurityKeyProvider>().As<ISecurityKeyProvider>();
            r.Export<AuthService>().As<IAuthService>();
            r.Export<IpValidator>().As<IIpValidator>();
        }
    }
}