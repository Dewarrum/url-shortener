using Grace.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace UrlShortener.Services.Configurations
{
    public class GraceConfiguration : IConfigurationModule
    {
        private readonly IConfiguration configuration;

        public GraceConfiguration(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        
        public void Configure(IExportRegistrationBlock r)
        {
            r.AddModule(new DataProvidingConfigurationModule());
            r.AddModule(new UniqueValuesProvidingConfigurationModule());
            r.AddModule(new SecurityConfigurationModule());
        }
    }
}