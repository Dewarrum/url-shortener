using Grace.DependencyInjection;
using Serilog;
using UrlShortener.Data.Repositories.AuthInfoRepository;
using UrlShortener.Data.Repositories.LinkRepository;
using UrlShortener.Data.Repositories.UserRepository;

namespace UrlShortener.Services.Configurations
{
    public class DataProvidingConfigurationModule : IConfigurationModule
    {
        public void Configure(IExportRegistrationBlock r)
        {
            r.ExportFactory<StaticInjectionContext, ILogger>(LoggerFactory);
            r.Export<UserRepository>().As<IUserRepository>().Lifestyle.Singleton();
            r.Export<LinkRepository>().As<ILinkRepository>().Lifestyle.Singleton();
            r.Export<AuthInfoRepository>().As<IAuthInfoRepository>().Lifestyle.Singleton();
        }
        
        private ILogger LoggerFactory(StaticInjectionContext context)
        {
            return Log.Logger.ForContext("Context", context.TargetInfo?.InjectionType?.Name != null 
                ? $" [{context.TargetInfo.InjectionType.Name}]" 
                : " [Middleware]");
        }
    }
}