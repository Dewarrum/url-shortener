using Grace.DependencyInjection;
using UrlShortener.Services.ShortLinkService;

namespace UrlShortener.Services.Configurations
{
    public class UniqueValuesProvidingConfigurationModule : IConfigurationModule
    {
        public void Configure(IExportRegistrationBlock r)
        {
            r.Export<LinkIdGenerator>().As<ILinkIdGenerator>();
        }
    }
}