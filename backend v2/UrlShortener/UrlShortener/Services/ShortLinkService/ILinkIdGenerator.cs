namespace UrlShortener.Services.ShortLinkService
{
    public interface ILinkIdGenerator
    {
        string GenerateId();
    }
}