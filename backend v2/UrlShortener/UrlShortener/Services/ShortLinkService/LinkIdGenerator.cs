using System;
using System.Buffers;
using System.Threading;
using Microsoft.Extensions.Configuration;
using UrlShortener.Data.TreadSafeRandomService;

namespace UrlShortener.Services.ShortLinkService
{
    public class LinkIdGenerator : ILinkIdGenerator
    {
        private readonly IConfiguration configuration;
        private static readonly string encode32Chars = "0123456789ABCDEFGHIJKLMNOPQRSTUV";
        private readonly int length = 8;

        public LinkIdGenerator(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public string GenerateId()
        {
            var chars = new char[length];

            for (var i = 0; i < length; i++)
            {
                chars[i] = encode32Chars[ThreadSafeRandom.Next(encode32Chars.Length)];
            }

            return new string(chars);
        }
    }
}