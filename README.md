# UrlShortener

Service for making long links shorter. Just post a long link you want to make shorter and get a short one. Then you can follow that link and be redirected to original path.

## Client side

Client side implemented using [Angular 7](https://angular.io) and [ng-tailwindcss](https://github.com/tehpsalmist/ng-tailwindcss).

## Server side

Server side implemented using [ASP.NET Core](https://github.com/aspnet/AspNetCore) Web API. [PostgreSQL](https://www.postgresql.org/) as Database.