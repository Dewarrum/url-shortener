export interface LinkPostPayload {
  originalUrl: string;
  userId: string;
}
