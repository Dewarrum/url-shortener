export interface LinkGetPayload {
  originalUrl: string;
  shortUrl: string;
}
