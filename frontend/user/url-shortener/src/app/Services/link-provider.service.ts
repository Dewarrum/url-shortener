import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {LinkGetPayload} from '../Models/LinkGetPayload';

@Injectable({
  providedIn: 'root'
})
export class LinkProviderService {
  link = {shortUrl: null, originalUrl: null};
  linkChange = new BehaviorSubject<LinkGetPayload>(this.link);
  constructor() { }
  public setLink(link: LinkGetPayload) {
    this.linkChange.next(link);
  }
  public getLink(): BehaviorSubject<LinkGetPayload> {
    return this.linkChange;
  }
}
