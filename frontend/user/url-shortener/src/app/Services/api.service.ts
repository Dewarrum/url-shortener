import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LinkGetPayload} from '../Models/LinkGetPayload';
import {LinkProviderService} from './link-provider.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  appUrl = 'http://localhost:5000/api/';
  defaultGuid = '00000000-0000-0000-0000-000000000000';
  constructor(private http: HttpClient, private linkProvider: LinkProviderService) { }
  public createLink(originalUrl: string): Observable<LinkGetPayload> {
    const body = {
      originalUrl,
      userId: this.defaultGuid
    };
    const obs = this.http.post<LinkGetPayload>(this.appUrl + 'links/create', body);
    // obs.subscribe(l => this.linkProvider.setLink(l));
    return obs;
  }
}
