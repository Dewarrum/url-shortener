import { Component, OnInit } from '@angular/core';
import {LinkGetPayload} from '../../Models/LinkGetPayload';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isShortLinkReady = false;
  link: LinkGetPayload;
  constructor() { }

  ngOnInit() {
  }
  shortenTrigger() {
    this.isShortLinkReady = true;
  }
  linkTrigger(link: LinkGetPayload) {
    this.link = link;
  }
}
