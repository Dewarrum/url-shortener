import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ApiService} from '../../Services/api.service';
import {LinkGetPayload} from '../../Models/LinkGetPayload';
import {LinkProviderService} from '../../Services/link-provider.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  @Output() shorten = new EventEmitter();
  originalUrl: string;
  link: LinkGetPayload;
  @Output() linkEmitter = new EventEmitter<LinkGetPayload>();
  constructor(private api: ApiService, private linkProvider: LinkProviderService) { }

  ngOnInit() {
  }
  createLink() {
    console.log(this.originalUrl);
    this.api.createLink(this.originalUrl).subscribe(l => {
      this.link = l;
      this.linkProvider.setLink(l);
      this.linkEmitter.emit(l);
    });
    this.shorten.emit();
  }
}
