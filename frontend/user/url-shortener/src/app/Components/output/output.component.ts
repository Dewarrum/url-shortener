import {Component, Input, OnInit} from '@angular/core';
import {LinkGetPayload} from '../../Models/LinkGetPayload';
import {LinkProviderService} from '../../Services/link-provider.service';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent implements OnInit {
  link: LinkGetPayload;
  constructor(private linkProvider: LinkProviderService) { }

  ngOnInit() {
    this.linkProvider.getLink().subscribe(l => this.link = l);
  }
  copyToClip(text: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
}
